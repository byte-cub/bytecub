package com.bytecub.gateway.mq.excutor;

import com.bytecub.common.constants.BCConstants;
import com.bytecub.common.domain.gateway.mq.DeviceUpMessageBo;
import com.bytecub.gateway.mq.services.IUpMessageParseService;
import com.bytecub.mdm.cache.IMessageCountCache;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 *  * ByteCub.cn.  * Copyright (c) 2020-2021 All Rights Reserved.  *   * @author bytecub@163.com songbin
 *  * @version Id: DeviceMessageUpExcutor.java, v 0.1 2021-02-25  Exp $$  
 */
@Service
@Slf4j
public class DeviceMessageUpExecutor {

    @Autowired
    private IMessageCountCache messageCountCache;
    @Autowired
    private IUpMessageParseService messageParseService;


    /***
     * 具体线程
     */
    @Async(BCConstants.TASK.DEVICE_UP_MESSAGE_NAME)
    public void execute(DeviceUpMessageBo msg, Boolean needCount) {
        try {
            if(needCount){
                messageCountCache.todayTotalIncr();
            }
            messageParseService.processReport(msg);
        } catch (Exception e) {
            log.warn("内部队列消费异常", e);
        }

    }

}
