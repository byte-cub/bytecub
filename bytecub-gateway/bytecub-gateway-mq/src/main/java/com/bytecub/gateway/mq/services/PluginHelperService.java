package com.bytecub.gateway.mq.services;

import com.bytecub.common.domain.message.DeviceDownMessage;
import com.bytecub.common.enums.TopicTypeEnum;
import com.bytecub.gateway.mq.domain.TopicMsgBo;

/**
 * Created on 2021/8/24.
 *  和数据协议相关的封装
 * @author songbin songbin.sky@hotmail.com
 */
public interface PluginHelperService {
    TopicMsgBo encode(TopicTypeEnum topicTypeEnum, String deviceCode, DeviceDownMessage deviceDownMessage);
}
