package com.bytecub.gateway.mq.services;

import com.bytecub.common.domain.gateway.mq.DeviceUpMessageBo;
import com.bytecub.protocol.base.IBaseProtocol;

/**
 * Created on 2021/8/26.
 * 上报数据解析
 * @author songbin songbin.sky@hotmail.com
 */
public interface IUpMessageParseService {
    /**处理设备主动上报数据*/
    void processReport(DeviceUpMessageBo msg);
    /**处理回执消息*/
    void processReply(DeviceUpMessageBo msg);
    void rebuildMsg(DeviceUpMessageBo msg);
    IBaseProtocol queryProtocolByProductCode(String productCode);
}
