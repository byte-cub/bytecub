package com.bytecub.udp.boot;

import com.bytecub.udp.service.network.IBcUdpServer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PreDestroy;

/**
 *  * ByteCub.cn.
 *  * Copyright (c) 2020-2021 All Rights Reserved.
 *  * 
 *  * @author bytecub@163.com  songbin
 *  * @Date 2021/4/2  Exp $$
 *  
 */
@Service
@Slf4j
public class UdpBoot {
    @Autowired
    IBcUdpServer bcUdpServer;

    public void boot(){
        bcUdpServer.start();
    }

    @PreDestroy
    public void destroy(){
        bcUdpServer.stop();
    }
}
