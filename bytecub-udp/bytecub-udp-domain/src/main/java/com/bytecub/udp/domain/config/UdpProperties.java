package com.bytecub.udp.domain.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 服务配置
 * @author dell
 */
@Component
@ConfigurationProperties(prefix = "bc.udp.server")
@Data
public class UdpProperties {

	private boolean enable = true;
	private int port = 1883;
	private boolean useEpoll = false;

}
