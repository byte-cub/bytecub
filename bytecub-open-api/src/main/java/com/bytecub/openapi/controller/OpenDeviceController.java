package com.bytecub.openapi.controller;

import com.alibaba.fastjson.JSONObject;
import com.bytecub.common.constants.BCConstants;
import com.bytecub.common.domain.DataResult;
import com.bytecub.common.domain.dto.PageReqDto;
import com.bytecub.common.domain.dto.PageResDto;
import com.bytecub.common.domain.dto.request.DevBatchAddReqDto;
import com.bytecub.common.domain.dto.request.DevQueryReqDto;
import com.bytecub.common.domain.dto.request.device.*;
import com.bytecub.common.domain.dto.request.prop.TemplateReqDto;
import com.bytecub.common.domain.dto.response.device.DevicePageResDto;
import com.bytecub.common.domain.dto.response.device.DeviceRtHistoryResDto;
import com.bytecub.common.domain.dto.response.device.DeviceRtResDto;
import com.bytecub.common.domain.dto.response.device.GwDevicePageResDto;
import com.bytecub.common.domain.dto.response.prop.TemplateResDto;
import com.bytecub.common.domain.gateway.direct.response.PropertyGetResponse;
import com.bytecub.common.domain.gateway.mq.MQSendMessageBo;
import com.bytecub.common.enums.BCErrorEnum;
import com.bytecub.common.enums.BatchOpEnum;
import com.bytecub.common.enums.DeviceStateEnum;
import com.bytecub.common.exception.BCGException;
import com.bytecub.common.metadata.ProductFuncTypeEnum;
import com.bytecub.gateway.direct.service.IPropGetService;
import com.bytecub.gateway.direct.service.IServiceInvoke;
import com.bytecub.gateway.mq.mqttclient.BcPubMqttClient;
import com.bytecub.gateway.mq.redis.publish.PropertySetPublisher;
import com.bytecub.manager.service.IAdminDeviceService;
import com.bytecub.mdm.service.IDeviceService;
import com.bytecub.mdm.service.IProductFuncService;
import com.bytecub.mdm.service.IProductService;
import com.bytecub.mqtt.service.biz.MqttRemoteService;
import com.bytecub.storage.IDataCenterService;
import com.bytecub.storage.IMessageReplyService;
import com.bytecub.storage.entity.MessageReplyEntity;
import com.bytecub.utils.JSONProvider;
import com.bytecub.utils.ObjectCopyUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *  * ByteCub.cn.  * Copyright (c) 2020-2021 All Rights Reserved.  *   * @author bytecub@163.com songbin
 *  * @version Id: OpenDeviceController.java, v 0.1 2021-01-08  Exp $$  
 */
@Slf4j
@RestController
@RequestMapping(BCConstants.URL_PREFIX.OPEN_API + "device")
@Api(description = "开放平台设备管理")
public class OpenDeviceController {
    @Autowired
    IDeviceService deviceService;

    @Autowired
    IProductFuncService productFuncService;

    @Autowired
    IDataCenterService dataCenterService;
    @Autowired
    IAdminDeviceService adminDeviceService;
    @Autowired
    IProductService productService;
    @Autowired
    BcPubMqttClient bcPubMqttClient;
    @Autowired
    IPropGetService propGetService;
    @Autowired
    IMessageReplyService messageReplyService;
    @Autowired
    private IServiceInvoke serviceInvoke;
    @RequestMapping(value = "search", method = RequestMethod.POST)
    @ApiOperation(value = "分页查询设备列表", httpMethod = "POST", response = DataResult.class, notes = "分页查询设备列表")
    public DataResult<PageResDto<DevicePageResDto>> search(@RequestBody PageReqDto<DevQueryReqDto> searchPage) {
        PageResDto<DevicePageResDto> resDto = deviceService.queryByPage(searchPage);
        return DataResult.ok(resDto);
    }

    @RequestMapping(value = "list/codes", method = RequestMethod.POST)
    @ApiOperation(value = "根据编码列表批量查询设备", httpMethod = "POST", response = DataResult.class, notes = "根据编码列表批量查询设备")
    public DataResult<Map<String, DevicePageResDto>> listByCodes(@RequestBody List<String> deviceCodes) {
        Map<String, DevicePageResDto> map = deviceService.queryByCodes(deviceCodes);
        return DataResult.ok(map);
    }

    @RequestMapping(value = "template", method = RequestMethod.POST)
    @ApiOperation(value = "物模型数据模版", httpMethod = "POST", response = DataResult.class, notes = "物模型数据模版")
    public DataResult template(@Valid @RequestBody TemplateReqDto reqDto) {
        if (StringUtils.isEmpty(reqDto.getIdentifier()) && StringUtils.isEmpty(reqDto.getProductCode())) {
            log.warn("产品编码和标识符不能同时为空");
            throw BCGException.genException(BCErrorEnum.INVALID_PARAM, "产品编码和标识符不能同时为空");
        }
        TemplateResDto resDto = productFuncService.template(reqDto);
        return DataResult.ok(resDto);
    }

    @RequestMapping(value = "batchadd", method = RequestMethod.POST)
    @ApiOperation(value = "批量添加设备", httpMethod = "POST", response = DataResult.class, notes = "批量添加设备")
    public DataResult batchAdd(@RequestBody DevBatchAddReqDto reqDto) {
        adminDeviceService.batchCreate(reqDto);
        return DataResult.ok();
    }

    @RequestMapping(value = "import/file", method = RequestMethod.POST)
    @ApiOperation(value = "导入设备", httpMethod = "POST", response = DataResult.class, notes = "导入设备")
    public DataResult importFile(@RequestBody List<DeviceImportReqDto> reqDtoList) {
        adminDeviceService.importExcel(reqDtoList);
        return DataResult.ok();
    }

    @RequestMapping(value = "query/ids", method = RequestMethod.POST)
    @ApiOperation(value = "根据编码列表查询ID列表", httpMethod = "POST", response = DataResult.class, notes = "根据编码列表查询ID列表")
    public DataResult<List<Long>> queryIds(@RequestBody List<String> deviceCodes) {
        List<Long> ids = deviceService.queryIds(deviceCodes);
        return DataResult.ok(ids);
    }

    @RequestMapping(value = "create", method = RequestMethod.POST)
    @ApiOperation(value = "添加设备", httpMethod = "POST", response = DataResult.class, notes = "添加设备")
    public DataResult create(@RequestBody DevCreateReqDto reqDto) {
        deviceService.create(reqDto);
        return DataResult.ok();
    }

    @RequestMapping(value = "update", method = RequestMethod.POST)
    @ApiOperation(value = "更新设备", httpMethod = "POST", response = DataResult.class, notes = "更新设备")
    public DataResult update(@RequestBody DeviceUpdateReqDto reqDto) {
        deviceService.updateByCode(reqDto);
        return DataResult.ok();
    }

    @RequestMapping(value = "gw/sub/dev", method = RequestMethod.POST)
    @ApiOperation(value = "网关设备列表", httpMethod = "POST", response = DataResult.class, notes = "网关设备列表")
    public DataResult<PageResDto<GwDevicePageResDto>> gwSubDevice(@RequestBody PageReqDto<DevQueryReqDto> searchPage) {
        PageResDto<DevicePageResDto> resDto = deviceService.queryByPage(searchPage);
        PageResDto<GwDevicePageResDto> result = new PageResDto<>();
        List<GwDevicePageResDto> list = new ArrayList<>();
        for (DevicePageResDto device : resDto.getResultData()) {
            GwDevicePageResDto item = new GwDevicePageResDto();
            long total = deviceService.countByGwDevice(device.getDeviceCode(), DeviceStateEnum.TOTAL);
            long active = deviceService.countByGwDevice(device.getDeviceCode(), DeviceStateEnum.ACTIVE);
            ObjectCopyUtil.copyProperties(device, item);
            item.setDeviceTotal(total);
            item.setDeviceActive(active);
            list.add(item);
        }
        result.setResultData(list);
        return DataResult.ok(result);
    }

    @RequestMapping(value = "map", method = RequestMethod.POST)
    @ApiOperation(value = "子设备关联到网关设备", httpMethod = "POST", response = DataResult.class, notes = "子设备关联到网关设备")
    public DataResult map(@RequestBody GatewayMapReqDto dto) {
        if (!dto.getRemoveAction()) {
            DevicePageResDto gwDevice = deviceService.queryByDevCode(dto.getGwDeviceCode());
            if (null == gwDevice) {
                throw BCGException.genException(BCErrorEnum.RESOURCE_NOT_EXISTS, "网关设备不存在");
            }
        } else {
            dto.setGwDeviceCode("");
        }

        deviceService.mapGateway(dto);
        return DataResult.ok();
    }

    @RequestMapping(value = "status/change", method = RequestMethod.POST)
    @ApiOperation(value = "设备状态批量改变", httpMethod = "POST", response = DataResult.class, notes = "设备状态批量改变")
    public DataResult statusChange(@RequestBody BatchStatusReqDto reqDto) {
        if (CollectionUtils.isEmpty(reqDto.getList())) {
            return DataResult.fail("没有选中要改变的项");
        }
        deviceService.batchChangeStatus(reqDto.getList(), BatchOpEnum.explain(reqDto.getType()));
        deviceService.batchChangeStatus(reqDto.getList(), BatchOpEnum.explain(reqDto.getType()));
        List<DevicePageResDto> list = deviceService.queryByIds(reqDto.getList());
        for(DevicePageResDto item:list){
            MqttRemoteService.closeNetwork(item.getDeviceCode());
        }
        return DataResult.ok();
    }

    @RequestMapping(value = "info", method = RequestMethod.GET)
    @ApiOperation(value = "设备详情查看", httpMethod = "GET", response = DataResult.class, notes = "设备详情查看")
    public DataResult info(String deviceCode) {
        if (StringUtils.isEmpty(deviceCode)) {
            return DataResult.fail(BCErrorEnum.INVALID_PARAM);
        }
        DevicePageResDto dto = deviceService.queryByDevCode(deviceCode);
        return DataResult.ok(dto);
    }

    @RequestMapping(value = "runtime", method = RequestMethod.GET)
    @ApiOperation(value = "设备运行状态", httpMethod = "GET", response = DataResult.class, notes = "设备运行状态")
    public DataResult<List<DeviceRtResDto>> runtime(String deviceCode, String productCode, String type) {
        if (StringUtils.isEmpty(deviceCode) || StringUtils.isEmpty(productCode)) {
            return DataResult.fail(BCErrorEnum.INVALID_PARAM);
        }
        ProductFuncTypeEnum productFuncTypeEnum = ProductFuncTypeEnum.explain(type);
        if (null == productFuncTypeEnum) {
            productFuncTypeEnum = ProductFuncTypeEnum.PROP;
        }
        List<DeviceRtResDto> result = deviceService.queryRtByDevCode(deviceCode, productCode, productFuncTypeEnum);
        return DataResult.ok(result);
    }

    @RequestMapping(value = "runtime/item", method = RequestMethod.POST)
    @ApiOperation(value = "查看设备具体属性运行详情列表", httpMethod = "POST", response = DataResult.class, notes = "查看设备具体属性运行详情列表")
    public DataResult<PageResDto<DeviceRtHistoryResDto>>
        rtItem(@RequestBody PageReqDto<DeviceRtItemReqDto> searchPage) {
        PageResDto<DeviceRtHistoryResDto> pageResult = adminDeviceService.searchRtItem(searchPage);
        return DataResult.ok(pageResult);
    }

    @RequestMapping(value = "set/item", method = RequestMethod.POST)
    @ApiOperation(value = "查看设备属性设置历史", httpMethod = "POST", response = DataResult.class, notes = "查看设备属性设置历史")
    public DataResult<PageResDto<DeviceRtHistoryResDto>>
        setItem(@RequestBody PageReqDto<DeviceRtItemReqDto> searchPage) {
        PageResDto<DeviceRtHistoryResDto> pageResult = adminDeviceService.searchSetItem(searchPage);
        return DataResult.ok(pageResult);
    }
    @RequestMapping(value = "service/invokereply", method = RequestMethod.POST)
    @ApiOperation(value = "下发指令带回执", httpMethod = "POST", response = DataResult.class, notes = "下发指令带回执")
    public DataResult invokeReply(@Valid @RequestBody InvokeReqDto reqDto) {
        log.info("下发指令请求...");
        JSONObject jsonObject = JSONProvider.map2JSONObject(reqDto.getRemoteCommand());
        reqDto.setCommand(jsonObject);
        Map reply = serviceInvoke.invokeWithReply(reqDto);
        return DataResult.ok(reply);
    }
    @RequestMapping(value = "service/invoke", method = RequestMethod.POST)
    @ApiOperation(value = "下发指令", httpMethod = "POST", response = DataResult.class, notes = "下发指令")
    public DataResult invoke(@Valid @RequestBody InvokeReqDto reqDto) {
        log.info("下发指令请求...");
        JSONObject jsonObject = JSONProvider.map2JSONObject(reqDto.getRemoteCommand());
        reqDto.setCommand(jsonObject);
        String messageId = serviceInvoke.invokeNoReply(reqDto);
        return DataResult.ok(messageId);
    }

    @RequestMapping(value = "message/get", method = RequestMethod.GET)
    @ApiOperation(value = "根据messageId查询回执", httpMethod = "GET", response = DataResult.class, notes = "根据messageId查询回执")
    public DataResult messageId(String deviceCode, String messageId) {
        if (StringUtils.isEmpty(messageId) || StringUtils.isEmpty(deviceCode)) {
            BCGException.genException(BCErrorEnum.INVALID_PARAM);
        }
        MessageReplyEntity messageReplyEntity = messageReplyService.queryByMessageId(deviceCode, messageId);
        return DataResult.ok(messageReplyEntity);
    }

    @RequestMapping(value = "property/set", method = RequestMethod.POST)
    @ApiOperation(value = "属性设置", httpMethod = "POST", response = DataResult.class, notes = "属性设置")
    public DataResult propertySet(@Valid @RequestBody PropertySetReqDto reqDto) {
        log.info("属性设置请求 {}", reqDto);
        JSONObject jsonObject = JSONProvider.map2JSONObject(reqDto.getRemoteCommand());
        reqDto.setCommand(jsonObject);
        MQSendMessageBo bo = new MQSendMessageBo();
        bo.setIdentifier(reqDto.getIdentifier());
        bo.setDeviceCode(reqDto.getDeviceCode());
        DevicePageResDto devicePageResDto = deviceService.queryByDevCode(reqDto.getDeviceCode());
        bo.setProductCode(devicePageResDto.getProductCode());
        bo.setCommand(reqDto.getCommand());

        PropertySetPublisher.send(bo);
        return DataResult.ok();
    }

    @RequestMapping(value = "property/get", method = RequestMethod.POST)
    @ApiOperation(value = "属性读取", httpMethod = "POST", response = DataResult.class, notes = "属性读取")
    public DataResult propertyGet(@Valid @RequestBody PropertyGetReqDto reqDto) {
        log.info("属性读取请求...");
        Long start = System.currentTimeMillis();
        String deviceCode = reqDto.getDeviceCode();
        String identifier = reqDto.getIdentifier();
        Long timeout = reqDto.getTimeout();
        PropertyGetResponse propertyGetResponse = propGetService.fetchProperty(deviceCode, identifier, timeout);
        Object value = propertyGetResponse.getValue();
        Long dura = (System.currentTimeMillis() - start) / 1000;
        log.info("属性读取请求耗时:{}s", dura);
        return null == value ? DataResult.fail(BCErrorEnum.TIMEOUT) : DataResult.ok(propertyGetResponse.getValue());
    }

    /** 接口废弃，在线状态存储位置改了 */
    @RequestMapping(value = "online", method = RequestMethod.POST)
    @ApiOperation(value = "在线设备列表", httpMethod = "POST", response = DataResult.class, notes = "在线设备列表")
    @Deprecated
    public DataResult deviceOnline() {
        List<String> list = MqttRemoteService.onlineDevices();
        Map<String, Object> result = new HashMap<>();
        result.put("total", list.size());
        result.put("data", list);
        return DataResult.ok(result);
    }

    @RequestMapping(value = "runtime/multiple", method = RequestMethod.POST)
    @ApiOperation(value = "多个设备运行状态", httpMethod = "POST", response = DataResult.class, notes = "设备运行状态")
    public DataResult<Map<String, List<DeviceRtResDto>>> runtimeMultiple(@RequestBody QueryDevsRtReqDto reqDto) {
        if (CollectionUtils.isEmpty(reqDto.getDeviceCodes()) || StringUtils.isEmpty(reqDto.getProductCode())) {
            return DataResult.fail(BCErrorEnum.INVALID_PARAM);
        }
        ProductFuncTypeEnum productFuncTypeEnum = ProductFuncTypeEnum.explain(reqDto.getType());
        if (null == productFuncTypeEnum) {
            productFuncTypeEnum = ProductFuncTypeEnum.PROP;
        }
        Map<String, List<DeviceRtResDto>> result =
            deviceService.queryRtByDevCodes(reqDto.getDeviceCodes(), reqDto.getProductCode(), productFuncTypeEnum);
        return DataResult.ok(result);
    }
}
