package com.bytecub.manager.controller.firmware;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bytecub.common.constants.BCConstants;
import com.bytecub.common.domain.DataResult;
import com.bytecub.common.domain.dto.PageReqDto;
import com.bytecub.common.domain.dto.PageResDto;
import com.bytecub.common.domain.dto.request.firmware.FirmwareQueryReqDto;
import com.bytecub.mdm.dao.po.FirmwarePo;
import com.bytecub.mdm.service.IFirmwareService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

/**
 *  * ByteCub.cn.  * Copyright (c) 2020-2020 All Rights Reserved.  *   * @author bytecub@163.com songbin
 *  * @version Id: DeviceController.java, v 0.1 2020-12-22  Exp $$  
 */
@Slf4j
@RestController
@RequestMapping(BCConstants.URL_PREFIX.MGR + "firmware")
@Api(tags = "固件管理")
public class FirmwareController {

    @Autowired
    private IFirmwareService firmwareService;


    @RequestMapping(value = "create", method = RequestMethod.POST)
    @PreAuthorize("@ps.hasRole('admin')")
    @ApiOperation(value = "新增固件", httpMethod = "POST", response = DataResult.class, notes = "导入设备")
    public DataResult add(@RequestBody @Validated FirmwarePo reqDto) {
        firmwareService.create(reqDto);
        return DataResult.ok();
    }

    @RequestMapping(value = "update", method = RequestMethod.POST)
    @PreAuthorize("@ps.hasRole('admin')")
    @ApiOperation(value = "更新固件", httpMethod = "POST", response = DataResult.class, notes = "更新固件")
    public DataResult update(@RequestBody FirmwarePo reqDto) {
        firmwareService.updateById(reqDto);
        return DataResult.ok();
    }

    @RequestMapping(value = "search", method = RequestMethod.POST)
    @ApiOperation(value = "分页查询固件列表", httpMethod = "POST", response = DataResult.class, notes = "分页查询固件列表")
    public DataResult<PageResDto<FirmwarePo>> search(@RequestBody PageReqDto<FirmwareQueryReqDto> searchPage) {
        PageResDto<FirmwarePo> resDto = firmwareService.searchByPage(searchPage);
        return DataResult.ok(resDto);
    }

    @RequestMapping(value = "delete", method = RequestMethod.GET)
    @PreAuthorize("@ps.hasRole('admin')")
    @ApiOperation(value = "删除固件", httpMethod = "GET", response = DataResult.class, notes = "删除固件")
    public DataResult delete(Long id) {
        firmwareService.deleteById(id);
        return DataResult.ok();
    }

    @RequestMapping(value = "detail", method = RequestMethod.GET)
    @ApiOperation(value = "单个固件详情", httpMethod = "GET", response = DataResult.class, notes = "单个固件详情")
    public DataResult<FirmwarePo> detail(Long id) {

        return DataResult.ok(firmwareService.queryById(id));
    }
}
