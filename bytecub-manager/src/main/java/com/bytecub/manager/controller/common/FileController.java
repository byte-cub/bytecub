package com.bytecub.manager.controller.common;

import com.bytecub.common.biz.FileUploadBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.bytecub.common.constants.BCConstants;
import com.bytecub.common.domain.DataResult;
import com.bytecub.utils.file.FileUploadUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

/**
 *  * ByteCub.cn.  * Copyright (c) 2020-2020 All Rights Reserved.  *   * @author bytecub@163.com songbin
 *  * @version Id: DeviceController.java, v 0.1 2020-12-22  Exp $$  
 */
@Slf4j
@RestController
@RequestMapping(BCConstants.URL_PREFIX.MGR + "file")
@Api(tags = "文件管理")
public class FileController {
    @Autowired
    private FileUploadBiz fileUploadBiz;

    @RequestMapping(value = "upload", method = RequestMethod.POST)
    @PreAuthorize("@ps.hasRole('admin')")
    @ApiOperation(value = "上传文件", httpMethod = "POST", response = DataResult.class, notes = "上传文件")
    public DataResult uploadFile(@RequestParam("file") MultipartFile file) {

        try{
            return DataResult.ok(fileUploadBiz.uploadFile(file));
        }catch (Exception e){
            log.warn("上传文件异常", e);
            throw new RuntimeException(e);
        }


    }

}
