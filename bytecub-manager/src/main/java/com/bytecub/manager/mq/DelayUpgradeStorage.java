package com.bytecub.manager.mq;

import java.util.concurrent.DelayQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bytecub.common.domain.dto.request.upgrade.UpgradeReqDto;

/**
 * com.bytecub.manager.mq
 * project bytecub  bytecub.cn
 *
 * @author songbin songbin.sky@hotmaial.com
 * @date 2021/4/29
 */
public class DelayUpgradeStorage {
    private static final Logger logger = LoggerFactory.getLogger(DelayUpgradeStorage.class);
    private static DelayQueue<UpgradeReqDto> queue = new DelayQueue<>();

    public static void push(UpgradeReqDto upgradeReqDto){
        try{
            queue.offer(upgradeReqDto);
        }catch (Exception e){
            logger.warn("延迟升级push异常", e);
        }
    }

    public static UpgradeReqDto take(){
        try{
            return queue.take();
        }catch (Exception e){
            logger.warn("延迟升级取数据异常", e);
            return null;
        }

    }
}
