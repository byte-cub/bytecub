package com.bytecub.manager.mq;

import com.bytecub.common.constants.BCConstants;
import com.bytecub.common.domain.dto.request.upgrade.UpgradeReqDto;
import com.bytecub.manager.service.IUpgradeService;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * com.bytecub.manager.mq
 * project bytecub  bytecub.cn
 *
 * @author songbin songbin.sky@hotmaial.com
 * @date 2021/4/29
 */
@Slf4j
@Service
public class DelayUpgradeConsume {

    @Autowired
    private IUpgradeService upgradeService;

    @Async(BCConstants.TASK.DELAY_UPGRADE)
    public void consume(){
        while (true){
            try{
                UpgradeReqDto task = DelayUpgradeStorage.take();
                if(null == task){
                    continue;
                }
                Date date = task.getUpgradeTime();
                log.info("准备升级,设定时间{}-{}", task.getUpgradeTime(), date);
                upgradeService.sendUpgrade(task);
            }catch (Exception e){
                log.warn("延迟队列消费异常", e);
                continue;
            }

        }
    }
}
