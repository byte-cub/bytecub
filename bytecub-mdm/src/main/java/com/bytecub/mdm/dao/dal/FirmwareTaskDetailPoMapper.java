package com.bytecub.mdm.dao.dal;

import com.bytecub.mdm.core.BaseDao;
import com.bytecub.mdm.dao.po.FirmwareTaskDetailPo;

import java.util.List;

public interface FirmwareTaskDetailPoMapper extends BaseDao<FirmwareTaskDetailPo> {

    List<FirmwareTaskDetailPo> query(FirmwareTaskDetailPo query);

}