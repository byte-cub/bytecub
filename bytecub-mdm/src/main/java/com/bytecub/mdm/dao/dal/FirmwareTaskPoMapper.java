package com.bytecub.mdm.dao.dal;

import com.bytecub.mdm.core.BaseDao;
import com.bytecub.mdm.dao.po.FirmwareTaskPo;

public interface FirmwareTaskPoMapper extends BaseDao<FirmwareTaskPo> {
    int deleteById(Long id);


}