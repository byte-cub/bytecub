package com.bytecub.mdm.dao.dal;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bytecub.common.domain.dto.request.firmware.FirmwareQueryReqDto;
import com.bytecub.mdm.core.BaseDao;
import com.bytecub.mdm.dao.po.FirmwarePo;

public interface FirmwarePoMapper extends BaseDao<FirmwarePo> {
    int deleteById(Long id);
    FirmwarePo queryById(Long id);
   // FirmwarePo selectByPrimaryKey(Long id);
    List<FirmwarePo> queryByPage(@Param("item") FirmwareQueryReqDto dto, @Param("startId") Integer startId, @Param("size") Integer pageSize);
    /**
     * 计算数量
     * @param dto
     * @return
     * */
    Long countByPage(@Param("item") FirmwareQueryReqDto dto);
}