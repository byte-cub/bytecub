package com.bytecub.mdm.service;

import com.bytecub.common.domain.gateway.mq.UpgradeMessageBo;
import com.bytecub.common.enums.UpgradeEnum;

/**
 * com.bytecub.mdm.service
 * project bytecub  bytecub.cn
 *
 * @author songbin songbin.sky@hotmaial.com
 * @date 2021/4/14
 */
public interface ITaskDetailService {
    /**
     * 返回版本号
     * */
    String update(UpgradeMessageBo msg, UpgradeEnum upgradeEnum, String extMsg);
}
