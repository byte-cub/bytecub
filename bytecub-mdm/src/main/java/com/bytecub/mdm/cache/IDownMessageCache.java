package com.bytecub.mdm.cache;

import com.bytecub.common.domain.message.DeviceDownMessage;

import java.util.List;

/**
 *  * ByteCub.cn.
 *  * Copyright (c) 2020-2021 All Rights Reserved.
 *  * 下行消息缓存
 * 针对的是UDP HTTP等下行网络协议
 *  * @author bytecub@163.com  songbin
 *  * @Date 2021/4/6  Exp $$
 *  
 */
public interface IDownMessageCache {
    void downMessageWriter(DeviceDownMessage messageBo);
    List<DeviceDownMessage> downMessageReader(String deviceCode);
}
